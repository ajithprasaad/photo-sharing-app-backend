const mongoose = require('mongoose')
const Schema = mongoose.Schema
mongoose.Promise = global.Promise
const md5 = require('md5')
const validator = require('validator')
const mongodbErrorHandler = require('mongoose-mongodb-errors')
const passportLocalMongoose = require('passport-local-mongoose')

const userSchema = new Schema({
  firstname: {
    type: String,
    required: 'Please suppy a first name',
    trim: true
  },
  lastname: {
    type: String,
    required: 'Please suppy a last name',
    trim: true
  },
  email: {
    type: String,
    unique: true,
    lowercase: true,
    trim: true,
    validate: {
      validator: value => {
        return validator.isEmail(value)
      },
      message: '{VALUE} is not a valid Email'
    },
    required: 'Please supply an email address'
  },
  mobile: {
    type: Number,
    unique: true,
    min: 10
  },
  password: String
})

userSchema.plugin(passportLocalMongoose, { usernameField: 'email' })
userSchema.plugin(mongodbErrorHandler)
module.exports = mongoose.model('User', userSchema)