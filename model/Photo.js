const mongoose = require('mongoose')
mongoose.Promise = global.Promise
const slug = require('slugs')

const photoSchema = new mongoose.Schema(
  {
    slug: String,
    name: {
      type: String,
      trim: true
    },
    description: {
      type: String,
      trim: true
    },
    photo: String,
    author: {
      type: mongoose.Schema.ObjectId,
      ref: 'User',
      required: 'You must supply an author'
    }
  }
)

photoSchema.pre('save', async function (next) {
    next()
})

module.exports = mongoose.model('Photo', photoSchema)
