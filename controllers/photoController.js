const mongoose = require('mongoose')
const Photo = mongoose.model('Photo')
const User = mongoose.model('User')
const multer = require('multer')
const fs = require('fs')
const uploadsFolder = 'uploads';

var storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './uploads');
  },
  filename: function (req, file, callback) {
    callback(null, file.fieldname + '-' + Date.now());
  }
});

var upload = multer({ storage: storage }).array('userPhoto', 2);


exports.photoForm = (req, res) => {
  res.render('Photo')
}

exports.photoUpload = (req, res) => {
  upload(req, res, function (err) {
    console.log(req.body);
    console.log(req.files);
    if (err) {
      return res.json({ message: "Error uploading" });
    }
    res.json({ message: "Files are uploaded" });
  });
}

exports.deletePhoto = function (req, res) {
  console.log(req.params.filename)
  fs.unlink('uploads/' + req.params.filename, function () {
    res.send({
      status: "200",
      responseType: "string",
      response: "success"
    })
  })
}

exports.getPhotos = (req, res) => {
  fs.readdir(uploadsFolder, (err, files) => {
    files.forEach(file => {
      console.log(file);
      return res.json({
        status: "Files found",
        files: files
      })
    })
  })
  res.json({ status: "No files found" })
}