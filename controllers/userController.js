const passport = require('passport')
const mongoose = require('mongoose')
const User = mongoose.model('User')
const promisify = require('es6-promisify')

/////////////////// VIEW ////////////////////

exports.loginForm = (req, res) => {
  res.render('login')
}

exports.registerForm = (req, res) => {
  res.render('signup')
}



///////////////// CONTROLLER ////////////////


exports.createUser = async (req, res) => {
  const user = await User.findOne({ email: req.body.email })
  res.json(
    {
      "status": "success",
      "user_id": user.id,
      "firstname": req.body['first-name'],
      "lastname": req.body['last-name'],
      "email": req.body.email,
      "mobile": req.body.mobile
    }
  )
}

exports.getAll = async (req, res) => {
  const user = await User.find()
  res.json(
    {
      "USERS": user,
    }
  )
}

exports.delete = async (req, res) => {
  const user = await User.findOneAndRemove({ _id: req.params.id }, (err) => {
    if (err) {
      return res.json({ "status": "User could not be removed. Make sure you have the correct user id", })
    }
    return res.json({ "status": "User removed successfully", })
  })
}

// UPDATE USERS METHOD
// exports.updateStore = async (req, res) => {
//   const user = await Store.findOneAndUpdate({ _id: req.params.id }, req.body, {
//     new: true,
//     runValidators: true
//   }).exec()
//   res.json({
//     success: true,
//     message: 'User details successfully updated'
//   })
// }

// REGISTER USERS METHOD
exports.register = async (req, res, next) => {
  const user = new User({
    firstname: req.body['first-name'], lastname: req.body['last-name'],
    email: req.body.email, mobile: req.body.mobile, password: req.body.password
  })
  const register = promisify(User.register, User)
  await register(user, req.body.password)
    .then((response) => {
      next()
    })
    .catch((err) => {
      res.status(409).json(
        {
          'status': "failure",
          'type': err.name,
          'message': err.message
        })
    })
}

// AUTHENTICATING USERS METHOD
exports.login = async (req, res, next) => {
  const user = await User.findOne({ email: req.body.email })
  if (!user) {
    return res.status(404).json({
      error: 'User Not Found'
    });
  }
  else if (req.body.password == user.password) {
    return res.status(200).render('photo');
  } else {
    return res.status(401).json({
      error: 'Invalid Password'
    });
  }
}

