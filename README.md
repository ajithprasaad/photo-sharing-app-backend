# photo-sharing-app-backend

SERVER : http://localhost:5555/

GET http://localhost:5555/register -> Register Form

GET http://localhost:5555/, http://localhost:5555/login -> Login Form

GET http://localhost:5555/api/users/all -> Lists all users

GET http://localhost:5555/api/photos -> Lists all photos

POST http://localhost:5555/api/users/delete/:id -> pass params(user-id) from POSTMAN to delete user

POST http://localhost:5555/api/photo -> choose photos and send it in message body using POSTMAN

POST http://localhost:5555/api/photo/:filename/delete -> pass params(filename) from postman to delete the uploaded photo