const express = require('express')
const router = express.Router()
const userController = require('../controllers/userController')
const photoController = require('../controllers/photoController')

// Do work here
router.get(['/', '/login'], userController.loginForm)
router.get('/register', userController.registerForm)

// Create users 
router.post('/api/register', userController.register, userController.createUser)

// Read users 
router.get('/api/users/all', userController.getAll)

// Delete users
router.get('/api/users/delete/:id', userController.delete)

// Update users
// router.post('/api/register/update/:id', userController.updateUser)

// Authenticate users
router.post('/api/login',userController.login)

// File fetching
router.get('/api/photos',photoController.getPhotos);

// File upload
router.post('/api/photo',photoController.photoUpload);

// File remove
router.post('/api/photo/:filename/delete',photoController.deletePhoto);

module.exports = router

